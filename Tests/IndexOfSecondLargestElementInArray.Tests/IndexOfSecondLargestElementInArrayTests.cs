﻿using FluentAssertions;

namespace IndexOfSecondLargestElementInArray.Tests
{
    public class IndexOfSecondLargestElementInArrayTests
    {
        [Fact]
        public void IndexOfSecondLargestElementInArray_ShouldReturnNegative1_WhenParamIsEmpty()
        {
            // arrange
            var param = Array.Empty<int>();
            var expectedIndex = -1;

            // act
            var result = Program.IndexOfSecondLargestElementInArray(param);

            // assert
            result.Should().Be(expectedIndex);
        }

        [Fact]
        public void IndexOfSecondLargestElementInArray_ShouldReturnNegative1_WhenParamHaveOnlyOneElement()
        {
            // arrange
            var param = new int[] { 1 };
            var expectedIndex = -1;

            // act
            var result = Program.IndexOfSecondLargestElementInArray(param);

            // assert
            result.Should().Be(expectedIndex);
        }

        [Fact]
        public void IndexOfSecondLargestElementInArray_ShouldReturnCorrect_WhenParamHave2Element()
        {
            // arrange
            var param = new int[] { -1, 2 };
            var expectedIndex = 0;

            // act
            var result = Program.IndexOfSecondLargestElementInArray(param);

            // assert
            result.Should().Be(expectedIndex);
        }

        [Fact]
        public void IndexOfSecondLargestElementInArray_ShouldReturnCorrect_WhenParamHave3Element()
        {
            // arrange
            var param = new int[] { -1, 2, -5 };
            var expectedIndex = 0;

            // act
            var result = Program.IndexOfSecondLargestElementInArray(param);

            // assert
            result.Should().Be(expectedIndex);
        }

        [Fact]
        public void IndexOfSecondLargestElementInArray_ShouldReturnCorrect_WhenDuplicateLargest()
        {
            // arrange
            var param = new int[] { -1, 0, 1, 1 };
            var expectedIndex = 3;

            // act
            var result = Program.IndexOfSecondLargestElementInArray(param);

            // assert
            result.Should().Be(expectedIndex);
        }

        [Fact]
        public void IndexOfSecondLargestElementInArray_ShouldReturnCorrect_WhenDuplicateSecondLargest()
        {
            // arrange
            var param = new int[] { 5, 1, 1 };
            var expectedIndex = 1;

            // act
            var result = Program.IndexOfSecondLargestElementInArray(param);

            // assert
            result.Should().Be(expectedIndex);
        }
    }
}
